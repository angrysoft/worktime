import 'dart:html';

void login() {
  InputElement user = querySelector('#login');
  InputElement pass = querySelector('#password');
  user.setCustomValidity('');
  pass.setCustomValidity('');
  if (user.reportValidity() && pass.reportValidity()) {
    FormData data = new FormData();
    data.append('login', user.value);
    data.append('password', pass.value);
  
    HttpRequest.request('/login', method: 'POST', sendData: data).then((HttpRequest resp) {
      if (resp.responseText == 'error') {
        user.setCustomValidity('email albo hasło niepoprawne');
        user.reportValidity();
      } else {
        String next = Uri.base.queryParameters['next'];
        if (next.runtimeType != String) {
          next =  '/';
        }
        window.location.replace(next);
      }
    });
  }
  
}

void main() {
  HtmlElement btnLogin = querySelector('#btn-login');
  btnLogin.onClick.listen((Event e) => login());
}