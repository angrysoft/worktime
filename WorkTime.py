#!/usr/bin/python3

import os
import json
from flask import Flask
from flask import request
from flask import redirect
from flask import render_template
from flask import url_for
from flask import session
from databases import MySqlDatabase
from models import *
from functools import wraps
from WorkTimeLib import LoginManager


app = Flask(__name__)


def login_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if not login_manager.is_logged():
            return redirect(url_for('login', next=request.url))
        return func(*args, **kwargs)
    return decorated_function


@app.route('/')
@login_required
def index():
    # return current month
    return render_template('worktime.html')


@app.route('/users')
@login_required
def users():
    users = db.select(Users).all()
    h = ('user_id', 'login', 'email')
    print(h, users)
    return render_template('show.html', headers=h, items=users)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        login = request.form['login']
        password = request.form['password']
        user = db.select(Users).where(Users.login == login).first()
        print(login,password,user)
        if user and user.check_password(password):
            login_manager.login_user(user)
            print(request.args)
            return request.args.get('next', '/')
        else:
            return 'error'
    elif request.method == 'GET':
        if login_manager.is_logged():
            return redirect('/')
        return render_template('login.html')

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    # session.pop('username', None)
    session.clear()
    return redirect(url_for('login'))

# ************* Main ************* #
with open('db/config.json', 'r') as jfile:
    config = json.load(jfile)

app.secret_key = os.urandom(12)
db = MySqlDatabase(config)
login_manager = LoginManager()

if __name__ == '__main__':
    app.run()
