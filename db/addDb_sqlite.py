#!/usr/bin/python3

import csv
import json
from hashlib import sha256
import sys
sys.path.append('../')
from angrysql.sqlitedb import Connection
from models import *


class UsersList:
    def __init__(self, filename):
        self.users = list()
        with open(filename, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for r in reader:
                email = '{}.{}@ves.pl'.format(r.get('name'), r.get('lastname'))
                email = email.lower()
                password = '{}{}'.format(r.get('name')[:2], r.get('lastname'))
                self.users.append({
                    'type': r.get('type'),
                    'name': r.get('name'),
                    'lastname': r.get('lastname'),
                    'email': email,
                    'nickname': r.get('nickname'),
                    'login': email,
                    'password': self.getPassword(password)
                })

    @property
    def all(self):
        return self.users

    def getPassword(self, pwd):
        hs = sha256()
        hs.update(pwd.encode())
        return hs.hexdigest()


class RatesList:
    def __init__(self, filename):
        self.allrates = None
        with open(filename, 'r') as jfile:
            self.allrates = json.load(jfile)

    @property
    def rates(self):
        return self.allrates.get('rates')

    @property
    def addons(self):
        return self.allrates.get('addons')


if __name__ == "__main__":
    with open('config.json', 'r') as jfile:
        config = json.load(jfile)
    db = Connection(config, echo=False)

    rates = RatesList('rates_names.json')
    for r in rates.rates:
        rname = RateName()
        rname.name = r
        print('Rate name : ', rname.name)
        db.insert(rname)
    for a in rates.addons:
        aname = RateName()
        aname.name = a
        print('Addon name : ', aname.name)
        db.insert(aname)

    usr = UsersList('users.csv')
    rates_name = db.select(RateName).all()
    print(rates_name)

    for u in usr.all:
        user = Users()
        user.login = u.get('login')
        user.password = u.get('password')
        user.email = u.get('email')
        print('Insert user: ', user.login)
        db.insert(user)
        v = 0
    for usr in db.select(Users).all():
        for rn in rates_name:
            ur = UserRates()
            print(usr.user_id, usr.login, rn.name, rn.rate_name_id)
            ur.rate_name_id = rn.rate_name_id
            ur.user_id = usr.user_id
            v += 1
            ur.value = v
            db.insert(ur)
    db.commit()



