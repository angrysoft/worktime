#!/usr/bin/python3
import json
import sys
sys.path.append('../')
from models import *
from angrysql.sqlitedb import Connection


def info(msg):
    print('>>  {}'.format(msg))

if __name__ == '__main__':
    config = dict()
    admin = dict()
    with open('config.json', 'r') as jconf:
        config = json.load(jconf)

    db = Connection(config, echo=True)
    db.create_tables(Users, RateName, UserRates, WorkDays, Addons)

