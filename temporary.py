# import mysql.connector as mariadb
import MySQLdb
import datetime

class SqlBase:
    def __init__(self):
        self._sql = None

    @staticmethod
    def _list2QString(val):
        if type(val) == list:
            return ','.join(["'{}'".format(x) for x in val])
        else:
            return "'{}'".format(val)

    def where(self, condition):

    @property
    def sql(self):
        return self._sql

    @sql.setter
    def sql(self, value):
        self._sql = value

    def __str__(self):
        return self._sql

    def __repr__(self):
        return self._sql


class Select(SqlBase):
    def __init__(self, columns=[]):
        super(Select, self).__init__()
        if columns:
            self._sql = 'SELECT {} FROM {{table}}'.format(self._list2QString(columns))
        else:
            self._sql = 'SELECT * FROM {table}'


class Insert:
    pass

class Update:
    pass

class Delete:
    pass


class SqlQuery:
    def __init__(self, table):
        self.table = table
        self._sql = ''
        self._where = ''
        self._sort = ''
        self.select = Select()

    # def select(self, columns='*'):
    #     if not columns == '*':
    #         columns = self._list2QString(columns)
    #     self._sql = "SELECT {} FROM {}".format(columns, self.table)

    def insert(self, args):
        if not type(args) == dict:
            raise TypeError('agrs are not dict')
        self._sql = "INSERT INTO {} {}"

    @staticmethod
    def _list2QString(val):
        if type(val) == list:
            return ','.join(["'{}'".format(x) for x in val])
        else:
            return "'{}'".format(val)

    def sql(self):
        query = self._sql
        if self._where:
            query = '{} WHERE {}'.format(query, self.where)
        if self._sort:
            query = '{} ORDER BY {}'.format(query, self.sort)
        return query

    @property
    def where(self):
        conditions = list()
        for c in self._where:
            conditions.append("{}='{}'".format(c, self._where[c]))
        return ' AND '.join(conditions)

    @where.setter
    def where(self, value):
        if not type(value) == dict:
            raise TypeError('This is not a dict')
        self._where = value

    @property
    def sort(self):
        sortlist = list()
        for s in self._sort:
            sortlist.append('{} {}'.format(s, self._sort[s]))
        return ','.join(sortlist)

    @sort.setter
    def sort(self, value):
        if not type(value) == dict:
            raise TypeError('This is not a dict')
        self._sort = value


class DataBase:
    def __init__(self, config):
        self.conn = MySQLdb.connect(user=config.get('user'),
                                    password=config.get('password'),
                                    database=config.get('dbname'),
                                    charset='utf8')

    def query(self, sql, args=(), one=False):
        self.cur.execute(sql, args)
        if one:
            return self.cur.fetchone()
        else:
            return self.cur.fetchall()

    def exists(self, where={}):
        self.cur.execute('SELECT COUNT(1) FROM %s WHERE %s=%s LIMIT 1'.format(self.table, column, value))
        ret = self.cur.fetchall()
        if ret[0][0]:
            return True
        else:
            return False

    def delete(self, column, value):
        self.cur.execute("DELETE FROM {} WHERE {}='{}'".format(self.table, column, value))
        ret = self.query('SELECT ROW_COUNT()')
        self.commit()
        return ret[0][0]

    def getAll(self):
        return self.select()

    def select(self, columns='*', one=False, where={}, sort={}):
        sql = SqlQuery(self.table)
        sql.select(columns=columns)
        sql.where = where
        sql.sort = sort
        print(sql.sql())
        self.cur.execute(sql.sql())
        if one:
            rows = self.cur.fetchone()
        else:
            rows = self.cur.fetchall()
        ret = []
        for row in rows:
            fields = {}
            for idx, col in enumerate(self.cur.description):
                fields[col[0]] = row[idx]
            ret.append(fields)
        return ret

    def commit(self):
        self.conn.commit()

    @staticmethod
    def _list2QString(val):
        if type(val) == list:
            return ','.join(["'{}'".format(x) for x in val])
        else:
            return "'{}'".format(val)

    @staticmethod
    def _getTimeStamp():
        return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


class Users(DataBase):
    def __init__(self, connections):
        super(Users, self).__init__(connections)
        self.table = 'users'
        self.addRequired = ['login', 'password', 'email']

    def addUser(self, data):
        data = self._checkData(data)
        if data.get('status') == 'ok':
            if self.exists('login', data.get('login')):
                return 'exists'
            sql = 'INSERT INTO {}(login,password,email) VALUES(%s,%s,%s)'.format(self.table)
            self.query(sql, (data.get('login'),
                             data.get('password'),
                             data.get('email')))
            self.commit()
        return data.get('status')

    def getById(self, userId):
        return self.select(where={'user_id': userId})

    def getByLogin(self, login):
        return self.select(where={'login': login})

    def deleteUser(self, userId):
        self.delete('user_id', userId)

    def updateUser(self, data):
        data = self._checkData(data, update=True)
        if data.get('status') == 'ok':
            if not self.exists('login', data.get('login')):
                return ' not exists'

            sql = 'UPDATE {} SET login=%s,password=%s,email'.format(self.table)
            self.query(sql, (data.get('login'),
                             data.get('password'),
                             data.get('email')))
            self.commit()
        return data.get('status')

    def _checkData(self, data, update=False):
        status = 'ok'
        keys = ['login', 'password', 'email']
        if update:
            keys.extend(['user_id', 'basic_salary'])
        for k in keys:
            if not data.get(k):
                status = k

        data['status'] = status
        return data


class Rates(DataBase):
    def __init__(self, connections):
        super(Rates, self).__init__(connections)
        self.table = 'rates'

    def addNewRate(self, data):
        if not data.get('name'):
            return 'name'
        elif self.exists('name', data.get('name')):
            return 'exists'
        self.query('INSERT INTO rates(name,addon) VALUES(%s,%s)',
                   (data.get('name'),
                    data.get('addon')))
        self.commit()
        return 'ok'

    def addUserRate(self, data):
        rate = Rate(data)
        if self.exists('rate_id', rate.rateId):
            return 'exists'
        self.cur.execute('INSERT INTO user_rates(rate_id,user_id,value) VALUES(%s,%s,%s)',
                         (rate.rateId, rate.userId, rate.value))
        self.commit()
        return 'ok'

    def getUserRateById(self, userId):
        pass

    def deleteRate(self, rateId):
        pass

    def updateUserRate(self, data):
        rate = Rate(data)
        if not self.exists('rate_id', rate.rateId):
            return 'not exists'
        self.cur.execute('UPDATE user_rates SET(rate_id=%s,user_id=%s,value=%s)')

